'use strict';

var expect = require('chai').expect;
var randomVerse = require('../index');
var CaV = require('chapter-and-verse');

describe('#random-verse', function() {
    // Runs 100 random verses and tests to see if they are valid
    for (var i = 0; i < 100; i++) {
        it('should return a valid reference every time', function() {
            var result = CaV(randomVerse());
            console.log(result);
            expect(result.success).to.equal(true);
        });
    }
});

# Random verse
An NPM Module for getting a random Bible reference.

## Installation
Install by running `npm i random-verse`.

## Usage
```
const randomVerse = require('random-verse')
console.log(randomVerse())
```

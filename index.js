const num = require('bible-num-book');
const CaV = require('chapter-and-verse');

/**
     * Retrieve random Bible verse
     *
     * @return {String} returns Bible reference
*/
module.exports = function () {
    let bookNum = Math.floor((Math.random() * 66)); // Get a random number between 0 and 65
    let book = num(bookNum); // Get book name for random number
    let info = CaV(book);
    let chapter = Math.floor((Math.random() * info.book.chapters));
    let verse = Math.floor((Math.random() * info.book.versesPerChapter[chapter]) + 1);
    let reference = book + ' ' + (chapter + 1) + ':' + verse;
    return reference;
}
